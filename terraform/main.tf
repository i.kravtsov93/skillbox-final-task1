terraform {
    required_providers {
        yandex = {
            source = "yandex-cloud/yandex"
        }
    }
    required_version = ">= 0.13"
}

provider "yandex" {
  token     = var.token
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
  zone      = var.zone
}

data "yandex_compute_image" "my-ubuntu-2204-1"{
    family = "ubuntu-2204-lts"
}

resource "yandex_compute_instance" "vm-proj-1"{
    name        = "proj-1"
    platform_id = "standard-v1"
    zone        = "ru-central1-a"

    resources {
        cores   = 2
        memory  = 2
    }

    boot_disk {
        initialize_params {
            image_id = "${data.yandex_compute_image.my-ubuntu-2204-1.id}"            
        }
    }

    network_interface {
        subnet_id = yandex_vpc_subnet.my-sn-1.id
        nat = true
    }

    metadata = {
        ssh-keys = "ubuntu:${file("/home/ikravtsov/.ssh/yc_rsa.pub")}"
    }
}

resource "yandex_compute_instance" "vm-proj-2"{
    name        = "proj-2"
    platform_id = "standard-v1"
    zone        = "ru-central1-a"

    resources {
        cores   = 2
        memory  = 2
    }

    boot_disk {
        initialize_params {
            image_id = "${data.yandex_compute_image.my-ubuntu-2204-1.id}"            
        }
    }

    network_interface {
        subnet_id = yandex_vpc_subnet.my-sn-1.id
        nat = true
    }

    metadata = {
        ssh-keys = "ubuntu:${file("/home/ikravtsov/.ssh/yc_rsa.pub")}"
    }
}

resource "yandex_vpc_network" "default" {
    name = "network-1"
}

resource "yandex_vpc_subnet" "my-sn-1" {
    zone            = "ru-central1-a"
    network_id      = yandex_vpc_network.default.id
    v4_cidr_blocks  = ["10.1.10.0/24"]
}

resource "yandex_lb_target_group" "my-lb-tg-1" {
    name        = "my-target-group-1"
    region_id   = "ru-central1"

    target {
        subnet_id = "${yandex_vpc_subnet.my-sn-1.id}"
        address   = "${yandex_compute_instance.vm-proj-1.network_interface.0.ip_address}"
    }

    target {
        subnet_id = "${yandex_vpc_subnet.my-sn-1.id}"
        address   = "${yandex_compute_instance.vm-proj-2.network_interface.0.ip_address}"
    }
}

resource "yandex_lb_network_load_balancer" "my-nw-lb-1" {
    name = "my-network-load-balancer-1"

    listener {
        name = "my-listener-1"
        port = 80
    }

    attached_target_group {
        target_group_id = "${yandex_lb_target_group.my-lb-tg-1.id}"

        healthcheck {
            name = "http"
            http_options {
                port = 80
            }
        }
    }
}

output "internal_ip_address_vm-proj-1" {
  value = yandex_compute_instance.vm-proj-1.network_interface.0.nat_ip_address
}

output internal_ip_address_vm-proj-2 {
  value = yandex_compute_instance.vm-proj-2.network_interface.0.nat_ip_address 
}

output internal_ip_address_laod_balancer {
  value = yandex_lb_network_load_balancer.my-nw-lb-1.listener
}

